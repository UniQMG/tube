precision highp float;

uniform vec2 u_resolution;
uniform float u_current_time;
uniform float u_timestep;
uniform float u_item_size;
uniform sampler2D u_waveform;
uniform float u_wavecount;

void main() {
  float pos = (u_resolution.y - gl_FragCoord.y) / u_item_size;
  float timehere = pos * u_timestep;

  vec4 final = texture2D(u_waveform, vec2(pos / u_wavecount, 0));
  float amp = final.r;
  bool marked = final.g > 0.0;
  float power = max(amp, 0.025);
  float ypos = abs(u_resolution.x/2.0 - gl_FragCoord.x) / (u_resolution.x/2.0);

  vec4 foreground = u_current_time >= timehere
    ? marked
      ? vec4(0.00, 0.00, 1.00, 1.0)
      : vec4(1.00, 0.00, 0.00, 1.0)
    : marked
      ? vec4(0.73, 0.53, 0.73, 1.0)
      : vec4(0.73, 0.53, 0.53, 1.0);
  vec4 background = marked
    ? vec4(0.00, 0.00, 0.50, 1.0)
    : mod(pos, 2.0) > 1.0
      ? vec4(0.33, 0.33, 0.33, 1.0)
      : vec4(0.27, 0.27, 0.27, 1.0);
  gl_FragColor = power > ypos ? foreground : background;
}
