export default {
  track: null,
  speed: 50,
  generator: function*() {
    let i = 0;
    let ang = 180;
    while (true) {
      i++;

      ang += 36;
      ang %= 360;
      if (i % 10 == 0) {
        yield {
          type: 'jumppad',
          progress: i*5,
          length: 1,
          start: ang - 5,
          end: ang + 5
        }
        ang += 180;
        continue;
      }

      yield {
        type: 'orb',
        progress: i*5,
        length: 1,
        start: ang - 5,
        end: ang + 5
      };
    }
  }
}
