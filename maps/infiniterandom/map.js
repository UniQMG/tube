export default {
  track: null,
  speed: 50,
  tick: (game, dt) => {
    game.speed = 50 + Math.floor(game.progress / 1000) * 5;
  },
  generator: function*() {
    let start = 100;

    for (let i = 5; i < 30; i += 5) {
      let rel = i % 10 == 0 ? 90 : 0;
      yield {
        type: 'obstacle',
        progress: i * 5 + start,
        length: 1,
        start: rel,
        end: rel + 90
      };
      yield {
        type: 'obstacle',
        progress: i * 5 + start,
        length: 1,
        start: rel + 180,
        end: rel + 270
      };
    }

    for (let i = 30; i < 30 + 3*8; i++) {
      let ang = (36 * i)%360;
      yield {
        type: 'obstacle',
        progress: i * 5 + start,
        length: 2,
        start: ang,
        end: ang+45
      };
    }

    let i = 60;
    while (true) {
      i += 5;
      let center = Math.random() * 360;
      let width = Math.random() > 0.8
        ? Math.random() * 80 + 60
        : 30;
      yield {
        type: 'obstacle',
        progress: i * 5 + start,
        length: 1,
        start: center - width,
        end: center + width
      };
    }
  }
}
