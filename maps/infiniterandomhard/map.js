export default {
  track: null,
  speed: 100,
  generator: function*() {
    for (let i = 60; true; i += 5) {
      let center = Math.random() * 360;
      let width = Math.random() > 0.8
        ? Math.random() * 60 + 60
        : 30;
      yield {
        type: 'obstacle',
        progress: i * 5,
        length: 1,
        start: center - width,
        end: center + width
      };
    }
  }
}
