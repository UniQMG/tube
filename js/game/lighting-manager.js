export default class LightingManager {
  constructor(game) {
    this.lights = {};
  }

  registerLight(name) {
    this.lights[name] = {
      color: 0xFFFFFF,
      intensity: 1,
      started: 0,
      frame: this._defaultFrame(name)
    }
  }

  _defaultFrame(name) {
    return {
      duration: 1,
      color: 0xFFFFFF,
      intensity: 1,
      peakIntensity: 2,
      effect: 'on',
      light: name
    }
  }

  reset() {
    for (let light of Object.values(this.lights)) {
      light.color = 0xFFFFFF;
      light.intensity = 1;
      light.started = 0;
      light.frame = this._defaultFrame(light.frame.name)
    }
  }

  newFrame(progress, frame) {
    let light = this.lights[frame.light];
    light.frame = frame;
    light.started = progress;
    switch (frame.effect) {
      case 'on':
        light.color = frame.color;
        light.intensity = frame.intensity;
        break;
      case 'fade':
        light.color = frame.color;
        light.intensity = frame.peakIntensity;
        break;
      case 'off':
        light.intensity = 0;
        break;
    }
  }

  lightStatus(name, progress) {
    let light = this.lights[name];
    let ratio = (progress - light.started) / light.frame.duration;
    ratio = Math.min(Math.max(ratio, 0), 1);

    switch (light.frame.effect) {
      case 'on': case 'off':
        return light;

      case 'fade':
        light.intensity = (
          light.frame.intensity * ratio +
          light.frame.peakIntensity * (1 - ratio)
        );
        return light;
    }
  }
}
