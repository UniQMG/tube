import * as THREE from '../../lib/three.js';
import keys from '../../keys.js';

import light, { createLightPool } from './objects/light.js';
import jumppad from './objects/jumppad.js';
import obstacle from './objects/obstacle.js';
import orb from './objects/orb.js';
const spawnables = {
  light,
  jumppad,
  obstacle,
  orb,
  keyframe: () => ({
    dispose: () => null,
    mesh: null
  })
};

import makeTube from './static/tube.js';
import makeFrontLight from './static/front-light.js';
import makeRearLight from './static/rear-light.js';
import makePlayer from './static/player.js';

export default class Renderer {
  constructor(game, canvas) {
    this.game = game;
    this.canvas = canvas;
    this.renderer = new THREE.WebGLRenderer({ canvas, alpha: false });

    if (game.settings.get('shadows')) {
      this.renderer.shadowMap.enabled = true;
      this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    }

    const fov = 55, aspect = 1, near = 0.1, far = 1000;
    this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    this.scene = new THREE.Scene();
    this.objects = {};
    this.refit();
    game.on('frame', this.render.bind(this));

    this.loader = new THREE.TextureLoader();

    this.scene.fog = new THREE.FogExp2(0x000000, 0.015);
    game.settings.watch('fog', val => {
      switch (val) {
        case 'close': this.scene.fog.density = 0.015; break;
        case   'far': this.scene.fog.density = 0.008; break;
        case   'off': this.scene.fog.density = 0.000; break;
      }
    });

    this.tubeScale = 10;
    this.register('tubeContainer', () => {
      return new THREE.Object3D();
    });

    this.register('ambientLight', () => {
      let light = new THREE.AmbientLight(0x404040);
      game.settings.watch('ambientLight', val => {
        light.intensity = val ? 1 : 0;
      });
      return light;
    });

    createLightPool(this);
    makePlayer(this);
    makeTube(this);
    makeFrontLight(this);
    makeRearLight(this);

    this.objectMap = new Map();
    this._debugDisposeAll = function() {
      for (let { dispose } of this.objectMap.values())
        dispose();
      this.objectMap.clear();
    }
    game.on('objectSpawn', object => {
      let z = object.progress + object.length/2;
      let depth = object.length;
      let startAng = object.start / 180 * Math.PI;
      let endAng = object.end / 180 * Math.PI;

      let func = spawnables[object.type];
      if (!func) {
        console.warn('Unknown object type', object.type);
        return;
      }
      let { mesh, dispose } = func(this, object, z, depth, startAng, endAng);
      this.objectMap.set(object, { mesh, dispose });
    });
    game.on('objectDispose', object => {
      let disposer = this.objectMap.get(object);
      if (!disposer) {
        console.warn('Object not found');
        return;
      };
      disposer.dispose();
      this.objectMap.delete(object);
    });
    game.on('graze', object => {
      this.objectText(obstacle, 'Grazed +100', 'grazetext');
    });
    game.on('orb', object => {
      this.objectText(obstacle, '+250', 'orbtext');
    });

    // Camera management stuff
    game.on('frame', () => {
      if (keys.free) {
        let vector = new THREE.Vector3(0, 0, 0);
        let speed = keys.shift ? 2.5 : 0.5;
        vector.x = (keys.d - keys.a) * speed;
        vector.y = (keys.q - keys.e) * speed;
        vector.z = (keys.s - keys.w) * speed;
        vector.applyQuaternion(this.camera.quaternion);
        this.camera.position.add(vector);
      } else {
        game.settings.watch('camera', angle => {
          if (angle == 'angle1') {
            this.camera.position.z = 10;
            this.camera.position.y = 0.1;
            this.camera.lookAt(0, -1, 0);
          } else {
            this.camera.position.z = 10;
            this.camera.position.y = 2.5;
            this.camera.lookAt(0, 0, 0);
          }
        })
      }
    });
    const euler = new THREE.Euler(0, 0, 0, 'YXZ');
    keys.on('drag', (dx, dy) => {
      if (keys.free) {
        euler.setFromQuaternion(this.camera.quaternion);
        euler.x -= dy / 100;
        euler.y -= dx / 100;
        euler.x = Math.min(Math.max(euler.x, -Math.PI), Math.PI)
        this.camera.quaternion.setFromEuler(euler);
      }
    });
  }


  objectText(object, text, clazz) {
    let pos = new THREE.Vector3();
    pos = pos.setFromMatrixPosition(this.objects.player.matrixWorld);
    pos.project(this.camera);
    // hack, clean this up
    // app.$refs.game.createFloatingText(
    //   text,
    //   (pos.x * this.canvas.width/2) + this.canvas.width/2,
    //   -(pos.y * this.canvas.height/2) + this.canvas.height/2,
    //   2000,
    //   clazz
    // );
  }

  register(container, name, callback) {
    if (!callback) {
      callback = name;
      name = container;
      container = this.scene;
    }

    this.objects[name] = callback();
    // console.log('register', { container, name, callback, object: this.objects[name] });
    this.objects[name].name = name;
    container.add(this.objects[name]);
    return this.objects[name];
  }

  refit() {
    this.width = this.canvas.width;
    this.height = this.canvas.height;
  }

  render(time) {
    let { tubeContainer, player, tube } = this.objects;

    tubeContainer.position.z = this.game.progress;
    tubeContainer.rotation.z = (-this.game.position - 90) / 360 * Math.PI * 2;
    tube.position.z = -this.game.progress;

    this.scene.rotation.z = -this.game.overtilt / 180 * Math.PI;
    let easing = Math.sqrt(1 - Math.pow(this.game.animateY - 1, 2));
    this.scene.rotation.z += easing * Math.PI;

    let base = -this.tubeScale/2 + 1/2;
    player.position.y = base + (-base * 2 * this.game.animateY);

    if (this.game.flipped) {
      tubeContainer.rotation.z += Math.PI;
      player.position.y = -base;
    }

    this.renderer.render(this.scene, this.camera);
  }
}
