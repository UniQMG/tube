import * as THREE from '../../../lib/three.js';

// threejs recompiles the whole damn pipeline whenever a light is
// added or removed, so we use a pool to offset it.
const pool = [];
export function createLightPool(renderer) {
  renderer.game.settings.watch('lightPoolSize', poolSize => {
    console.log("poolsize update");
    for (let light of pool)
      renderer.objects.tubeContainer.remove(light);
    pool.length = 0;

    for (let i = 0; i < poolSize; i++) {
      const intensity = 1, distance = 100, decay = 2;
      let light = new THREE.PointLight(0x000000, intensity, distance, decay);
      renderer.objects.tubeContainer.add(light);
      light.name = `Light pool #${i}`
      pool.push(light);
    }
  })
}

export default function spawn(renderer, object, z, depth, startAng, endAng) {
  if (pool.length == 0) {
    console.warn('Light pool is empty.');
    return { mesh: null, dispose: () => {} }
  }
  const light = pool.pop();
  light.color.set(object.light.color);
  light.intensity = object.light.intensity;
  light.distance = object.light.distance;
  light.position.z = -z;
  let dispose = (() => {
    light.color.set(0x000000);
    pool.push(light);
  });
  return { mesh: light, dispose };
}
