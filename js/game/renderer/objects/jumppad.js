import * as THREE from '../../../lib/three.js';

const innerRadius = 1.1;
const outerRadius = 1.5;
const segments = 3;
const geometry = new THREE.RingBufferGeometry(
  innerRadius, outerRadius, segments
);
const material = new THREE.MeshPhongMaterial({
  color: 0xd65a31
});

export default function spawn(renderer, object, z, depth, startAng, endAng) {
  let mesh = new THREE.Mesh(geometry, material);
  let ang = (startAng + endAng)/2 + Math.PI*1;
  let scalar = -renderer.tubeScale/2 + 1/2;
  mesh.rotation.y = Math.PI * 0.15;
  mesh.position.x = Math.cos(ang) * scalar;
  mesh.position.y = Math.sin(ang) * scalar;
  mesh.position.z = -z;
  renderer.objects.tubeContainer.add(mesh);
  let dispose = (() => {
    renderer.objects.tubeContainer.remove(mesh);
  });
  return { mesh, dispose };
}
