import * as THREE from '../../../lib/three.js';

const radius = 0.5;
const detail = 0;
const geometry = new THREE.IcosahedronBufferGeometry(radius, detail);
const material = new THREE.MeshPhongMaterial({
  emissive: 0xC7BB18
});

export default function spawn(renderer, object, z, depth, startAng, endAng) {
  let mesh = new THREE.Mesh(geometry, material);
  mesh.castShadow = true;
  let ang = (startAng + endAng)/2 + Math.PI*1;
  let scalar = -renderer.tubeScale/2 + 1/2;
  mesh.position.x = Math.cos(ang) * scalar;
  mesh.position.y = Math.sin(ang) * scalar;
  mesh.position.z = -z;
  renderer.objects.tubeContainer.add(mesh);
  let dispose = (() => {
    renderer.objects.tubeContainer.remove(mesh);
  });
  return { mesh, dispose };
}
