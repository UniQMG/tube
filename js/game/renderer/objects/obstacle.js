import * as THREE from '../../../lib/three.js';

const material = new THREE.MeshPhongMaterial({
  color: 0xFFFFFF
});

export default function spawn(renderer, object, z, depth, startAng, endAng) {
  const shape = new THREE.Shape();
  const innerRad = renderer.tubeScale-6;
  const outerRad = renderer.tubeScale;
  shape.absarc(0, 0, outerRad, startAng, endAng);
  shape.lineTo(Math.cos(endAng) * innerRad, Math.sin(endAng) * innerRad);
  shape.absarc(0, 0, innerRad, endAng, startAng, true);

  const geometry = new THREE.ExtrudeBufferGeometry(shape, {
    depth,
    bevelEnabled: false
  });

  let mesh = new THREE.Mesh(geometry, material);
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  mesh.position.z = -z;
  renderer.objects.tubeContainer.add(mesh);
  let dispose = (() => {
    renderer.objects.tubeContainer.remove(mesh);
    geometry.dispose();
  });
  return { mesh, dispose };
}
