import * as THREE from '../../../lib/three.js';

export default function spawn(renderer) {
  renderer.register('rear-light', () => {
    const color = 0xFFFFFF;
    const intensity = 1;
    const distance = 100;
    const decay = 2;
    const angle = Math.PI * 2/3;
    const penumbra = 1;

    const light = new THREE.SpotLight(color, intensity, distance, angle, penumbra, decay);
    light.castShadow = true;

    light.target = renderer.register('rear-light-target', () => {
      const object = new THREE.Object3D();
      object.position.set(0, 0, 0);
      return object;
    });
    light.position.set(0, 0, 1);

    light.shadow.mapSize.width = 512;
    light.shadow.mapSize.height = 512;
    light.shadow.camera.fov = angle;
    light.shadow.bias = -0.0001;

    renderer.game.on('frame', dt => {
      if (renderer.game.alive) {
        let game = renderer.game;
        let gamelight = game.lighting.lightStatus('main', game.progress);
        light.color.set(gamelight.color);
        light.intensity = gamelight.intensity;
      } else {
        light.color.set(0xCC1919);
        light.intensity = 1;
      }
    });

    return light;
  });
}
