import * as THREE from '../../../lib/three.js';

export default function spawn(renderer) {
  renderer.register('front-light', () => {
    const color = 0xFFFFFF;
    const intensity = 1;
    const distance = 100;
    const decay = 2;
    const angle = 15 * Math.PI/180;
    const penumbra = 1;

    const light = new THREE.SpotLight(color, intensity, distance, angle, penumbra, decay);
    light.castShadow = true;
    light.position.set(0, 0, -100);

    light.shadow.mapSize.width = 512;
    light.shadow.mapSize.height = 512;
    light.shadow.camera.fov = angle;
    light.shadow.bias = -0.0001;
    light.shadow.camera.updateProjectionMatrix();
    return light;
  });
}
