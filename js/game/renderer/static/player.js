import * as THREE from '../../../lib/three.js';

export default function spawn(renderer) {
  renderer.register('player', () => {
    const geometry = new THREE.BoxBufferGeometry(1, 1, 1);
    const material = new THREE.MeshPhongMaterial({ color: 0x20bce3 });
    const cube = new THREE.Mesh(geometry, material);
    // cube.castShadow = true;
    return cube;
  });
}
