import * as THREE from '../../../lib/three.js';

export default function tube(renderer) {
  renderer.register(renderer.objects.tubeContainer, 'tube', () => {
    const radiusTop = renderer.tubeScale;
    const radiusBottom = renderer.tubeScale;
    const cylHeight = 400;
    const radialSegments = 36;
    const heightSegments = 1;
    const openEnded = true;
    const geometry = new THREE.CylinderBufferGeometry(
      radiusTop, radiusBottom, cylHeight, radialSegments, heightSegments, openEnded
    );

    const texture = renderer.loader.load('res/texture/tube.png');
    texture.repeat.set(0.5, cylHeight/200);
    texture.wrapT = THREE.RepeatWrapping;

    let tx = 0;
    renderer.game.on('frame', dt => {
      tx += renderer.game.speed / 50 * dt / 10000;
      texture.offset.set(0, -(tx%1));
    });
    const material = new THREE.MeshPhongMaterial({
      side: THREE.BackSide,
      color: 0xFFFFFF,
      map: texture
    });
    const tube = new THREE.Mesh(geometry, material);
    // const tube = new THREE.LineSegments(new THREE.WireframeGeometry(geometry));
    tube.position.z = 0;
    tube.rotation.x = Math.PI/2;
    tube.receiveShadow = true;

    return tube;
  });
}
