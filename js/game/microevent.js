export default class MicroEvent {
  constructor() {
    this.events = {};
  }

  registerEvent(event) {
    this.events[event] = [];
  }

  emit(event, ...args) {
    if (!this.events[event])
      throw new Error('Unknown event ' + event);
    for (let handler of this.events[event])
      handler(...args);
  }

  on(event, handler) {
    if (!this.events[event])
      throw new Error('Unknown event ' + event);
    this.events[event].push(handler);
    return handler;
  }

  off(event, handler) {
    if (!this.events[event])
      throw new Error('Unknown event ' + event);
    let index = this.events[event].indexOf(handler);
    if (index == -1) return false;
    this.events[event].splice(index, 1);
    return true;
  }
}
