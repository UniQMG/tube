import keys from '../keys.js';
import MicroEvent from './microevent.js';
import Renderer from './renderer/renderer.js';
import LightingManager from './lighting-manager.js';
import settings from './settings.js';

export default class Game extends MicroEvent {
  constructor(canvas) {
    super();
    this.canvas = canvas;

    // External editor
    this._editing = false;
    this.settings = settings;

    // Constants
    this.playerWidth = 20;
    this.playerLength = 1;

    // Game vars
    this.progress = 0;
    this.position = 0;
    this.objects = [];
    this.score = 0;
    this.alive = true;

    // Animation vars
    this.overtilt = 0; // in degrees, visually tilts entire tube including player
    this.animateY = 0; // 0-1 scalar controlling player height, used for jumppad
    this.flipped = false; // yes

    // Map management
    this.map = null;
    this.speed = 0;
    this.audio = document.createElement('audio');
    this.audio.addEventListener('timeupdate', () => {
      if (this.editing || !this.alive || this.audio.paused || !this.track)
        return;
      this.progress = this.audio.currentTime * this.speed;
    });

    // Object spawning
    this.generator = function*() {};
    this.nextObject = null;

    // Misc
    this._paused = true;
    this.lastFrame = 0;
    this.frame = this.frame.bind(this);

    this.lighting = new LightingManager();
    this.lighting.registerLight('main');

    this.registerEvent('frame');
    this.registerEvent('objectSpawn');
    this.registerEvent('objectDispose');
    this.registerEvent('graze');
    this.registerEvent('dead');
    this.registerEvent('orb');

    this.on('graze', obj => this.score += 100 * !this.settings.get('invincible'));
    this.on('orb', obj => this.score += 250 * !this.settings.get('invincible'));
    this.on('dead', () => {
      console.log("Dead!");
      this.alive = false;
      this.audio.pause();
      setTimeout(() => {
        this.restart();
      }, 2500);
    });

    this.on('frame', this.step.bind(this));
    this.renderer = new Renderer(this, canvas);

    requestAnimationFrame(this.frame);
  }
  get editing() {
    return this._editing
  }
  set editing(val) {
    this._editing = val;
    this.audio.volume = val ? 0 : 1;
  }
  get paused() { return this._paused; }
  set paused(val) {
    val ? this.audio.pause() : this.audio.play();
    this._paused = val;
  }
  scrubTo(time) {
    if (isNaN(time)) {
      debugger;
      return;
    }
    this.audio.currentTime = time;
    this.progress = time * this.speed;
  }
  setMapData(map) {
    this.map = map;
    this.restart();
  }
  get track() { return this.map?.track || null; }
  get mapdata() { return this.map?.mapdata || null; }
  stop() {
    this.map = null;
    this.restart();
    this.audio.src = null;
  }
  restart() {
    for (let object of this.objects.splice(0))
      this.emit('objectDispose', object);
    this.progress = 0;
    this.position = 180 + 1e-2;
    this.score = 0;

    if (this.map) {
      this.generator = this.map.generator();
      this.speed = this.map.speed;
      this.nextObject = this.generator.next().value;
    }
    this.lastFrame = 0;

    this.lighting.reset();
    this.paused = false;

    if (this.track) {
      this.audio.currentTime = 0;
      if (this.audio.getAttribute('src') != this.track)
        this.audio.src = this.track;
      this.audio.load();
      const that = this;
      this.audio.addEventListener('canplay', function handler() {
        that.audio.removeEventListener('canplay', handler);
        that.audio.play();
        that.alive = true;
      });
    } else {
      this.alive = true;
    }
  }
  step(dt) {
    let vol = this._editing ? 0 : this.settings.get('volume');
    if (this.audio.volume != vol)
      this.audio.volume = vol;

    // Object spawning and despawning
    while (this.nextObject && this.progress + 200 > this.nextObject.progress) {
      this.objects.push(this.nextObject);
      this.emit('objectSpawn', this.nextObject);
      this.nextObject = this.generator.next().value;
    }
    while (
      this.objects.length > 0 &&
      this.progress - 100 > this.objects[0].progress + this.objects[0].length
    ) this.deleteObject(this.objects[0]);

    if (!this.alive || this.paused) return;

    // Custom map scripting
    if (this.map && this.map.tick) this.map.tick(this, dt);

    let axis = 0;
    for (let entry of keys.axes(true))
      if (Math.abs(entry) > Math.abs(axis))
        axis = entry;

    // Controls and movement
    if (!keys.free && (!this.track || !this.audio.ended)) {
      this.score += dt/1000 * 100 * !this.settings.get('invincible');
      let dPos = axis * (360 * dt/1000);
      this.position = (this.position + dPos + 360) % 360;

      if (this.settings.get('enableOvertilt')) {
        if (dPos) {
          this.overtilt += dPos * 0.1;
          this.overtilt *= (1 - 0.01 * dt/1000*144);
        } else {
          this.overtilt *= (1 - 0.03 * dt/1000*144);
        }
      } else {
        this.overtilt = 0;
      }
      this.animateY *= (1 - 0.03 * dt/1000*144);

      // Overridden when a track is playing, but this interpolates it
      this.progress += dt/1000 * this.speed;
    }

    // Object updating
    for (let object of this.objects) {
      let angleHit = this.intersectsPlayerRotation(object, this.playerWidth*0.8);
      let grazing = this.intersectsPlayerRotation(object, this.playerWidth*2);
      let progressHit = (
        this.progress + this.playerLength > object.progress &&
        this.progress < object.progress + object.length
      );

      switch (object.type) {
        case 'obstacle':
          if (angleHit && progressHit && !this.editing) {
            if (!this.settings.get('invincible'))
              this.emit('dead');
          } else if (grazing && !angleHit && progressHit && !object.grazed) {
            object.grazed = true;
            this.emit('graze', object);
          }
          break;

        case 'orb':
          if (angleHit && progressHit) {
            this.emit('orb', object);
            this.deleteObject(object);
          }
          break;

        case 'jumppad':
          if (angleHit && progressHit) {
            // flipped = !flipped;
            // this.flipped = !this.flipped;
            this.animateY = 1;
            this.position = (this.position + 180) % 360;
          }
          break;

        case 'keyframe':
          if (this.progress >= object.progress) {
            this.lighting.newFrame(this.progress, object.keyframe);
            this.deleteObject(object);
          }
          break;
      }
    }
  }
  frame(time) {
    if (this.lastFrame == 0)
      this.lastFrame = time;
    let dt = time - this.lastFrame;
    this.lastFrame = time;
    this.emit('frame', dt);
    if (keys.r) {
      this.restart();
      return;
    }
    requestAnimationFrame(time => this.frame(time));
  }
  intersectsPlayerRotation(object, width=this.playerWidth) {
    let start = this.position + width/2 > object.start;
    let end = this.position - width/2 < object.end;
    if (object.start < 0 || object.end >= 360) {
      let startSpecial = this.position + width/2 > 360 + object.start;
      let endSpecial = this.position - width/2 < object.end - 360;
      return (startSpecial || end) && (endSpecial || start);
    }
    return start && end;
  }
  refreshObjects() { // Used by editor
    for (let object of this.objects) {
      this.emit('objectDispose', object);
      this.emit('objectSpawn', object);
    }
  }
  addObject(object) { // Used by editor
    for (let existing of this.objects)
      if (existing == object) return;
    this.objects.push(object);
    this.emit('objectSpawn', object);
  }
  updateObject(object) { // Used by editor
    this.addObject(object);
    this.emit('objectDispose', object);
    this.emit('objectSpawn', object);
  }
  deleteObject(object) {
    let index = this.objects.indexOf(object);
    if (!object || index == -1) return false;
    let retrieved = this.objects.splice(index, 1)[0];
    this.emit('objectDispose', retrieved);
    return true;
  }
  refit() {
    this.renderer.renderer.setSize(
      this.canvas.clientWidth,
      this.canvas.clientHeight,
      false
    );
    this.renderer.camera.aspect = (
      this.canvas.clientWidth / this.canvas.clientHeight
    );
    this.renderer.camera.updateProjectionMatrix();
  }
}
