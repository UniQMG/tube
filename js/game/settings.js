class SettingsManager {
  constructor() {
    this.settings = new Map();
  }

  all() {
    return this.settings.values();
  }

  load() {
    try {
      let loaded = JSON.parse(localStorage.settings);
      for (let [key, value] of Object.entries(loaded))
        if (!this.set(key, value))
          throw new Error('Invalid stored setting ' + key + ': ' + value);
    } catch(ex) {
      console.info('Unparsable or nonexistant settings', ex);
    }
  }

  save() {
    let obj = Object.fromEntries(this.settings);
    for (let key of Object.keys(obj))
      obj[key] = obj[key].value;
    localStorage.settings = JSON.stringify(obj);
  }

  provide(key, options) {
    this.settings.set(key, {
      name: key,
      restart: false,
      ...options,
      value: options.default,
      callbacks: []
    });
  }

  _getGuarded(key) {
    if (!this.settings.has(key))
      throw new Error('Unknown setting: ' + key);
    return this.settings.get(key);
  }

  watch(key, callback) {
    this._getGuarded(key).callbacks.push(callback);
    callback(this.get(key));
    return callback;
  }

  unwatch(key, callback) {
    let callbacks = this._getGuarded(key).callbacks;
    let index = callbacks.indexOf(callback);
    if (index == -1) return false;
    callbacks.splice(index, 1);
    return true;
  }

  get(key) {
    return this._getGuarded(key).value;
  }

  set(key, value) {
    let set = this._getGuarded(key);
    if (!this.validate(value, set))
      return false;
    for (let handler of set.callbacks)
      handler(value);
    set.value = value;
    return true;
  }

  validate(value, options) {
    switch (options.type) {
      case 'boolean':
        return value === true || value === false;

      case 'number':
        if (typeof options.max !== 'undefined' && value > options.max)
          return false;
        if (typeof options.min !== 'undefined' && value < options.min)
          return false;
        return true;

      case 'enum':
        return options.values.indexOf(value) !== -1;

      default:
        console.error('Unknown type:', options.type);
        return false;
    }
  }
}

let settings = new SettingsManager();
settings.provide('enableOvertilt', {
  comment: 'Sways tube to the side when sustaining rotation',
  type: 'boolean',
  default: true
});
settings.provide('invincible', {
  comment: 'Makes you invincible, but disables score',
  type: 'boolean',
  default: false
});
settings.provide('shadows', {
  comment: 'Enables shadows',
  restart: true,
  type: 'boolean',
  default: false
});
settings.provide('volume', {
  comment: 'Game music volume',
  type: 'number',
  default: 1,
  min: 0,
  max: 1
});
settings.provide('ambientLight', {
  comment: 'Baseline light to make the game brighter',
  restart: true,
  type: 'boolean',
  default: false
});
settings.provide('fog', {
  comment: 'Adjusts the black fog visibility distance',
  restart: true,
  type: 'enum',
  values: ['close', 'far', 'off'],
  default: 'close'
});
settings.provide('camera', {
  comment: 'Choose from multiple camera angles',
  type: 'enum',
  values: ['angle1', 'angle2'],
  default: 'angle1'
});
settings.provide('lightPoolSize', {
  comment: 'Fixed number of lights available for map lighting. Abnormally performance intensive.',
  type: 'number',
  default: 30,
  min: 0,
  max: 1000
})
settings.load();
export default settings;
