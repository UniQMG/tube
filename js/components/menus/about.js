const html = arg => arg.join('');

export default {
  template: html`
    <div>
      <button class="gamebutton small right" @click="back">
        🡄 back
      </button>

      <h1>About</h1>
      Tube (name subject to change) is a fairly simple rhythm game where you
      dodge obstacles coming at you in a closed tube by rotating around it. It
      runs entirely in browser and is still under early development. It supports
      mapping but currently lacks a way to share maps without manually pasting
      them into the editor.

      <h2>Credits</h2>
      Dev: UniQMG

      <h3>Music</h3>
      <div class="song">
        Distrion & Electro-Light - Rubik<br>
        <a href="https://www.youtube.com/watch?v=A2AydJcUKR8">
          https://www.youtube.com/watch?v=A2AydJcUKR8
        </a><br>
        © NCS
      </div>

      <h3>Third-party libraries</h3>
      <a href="https://github.com/mrdoob/three.js/">
        Three.js (MIT)
      </a>
      <br>
      <a href="https://github.com/ThibaultJanBeyer/DragSelect">
        DragSelect (MIT)
      </a>
      <br>
      <a href="https://interactjs.io/">
        Interact.js (MIT)
      </a>
      <br>
      <a href="https://vuejs.org/">
        Vue.js (MIT)
      </a>
      <br>
      <a href="https://github.com/patriciogonzalezvivo/glslCanvas">
        GlslCanvas (MIT)
      </a>
    </div>
  `,
  methods: {
    back() {
      this.$emit('navigate', 'mainmenu');
    }
  }
}
