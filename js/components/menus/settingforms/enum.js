const html = arg => arg.join('');

export default {
  template: html`
    <div class="form-group enum">
      <label :for="setting.name">
        {{ setting.name | decamel }}
      </label>
      <div class="enum-options">
        <button
          v-for="value of setting.values"
          :value="value"
          @click="input(value)"
          :disabled="setting.value == value"
        >
          {{value}}
        </button>
      </div>
      {{ setting.comment }}
    </div>
  `,
  props: ['setting'],
  methods: {
    input(val) {
      this.$emit('input', val);
    }
  }
};
