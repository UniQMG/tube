const html = arg => arg.join('');

export default {
  template: html`
    <div class="form-group boolean">
      <label :for="setting.name">
        {{ setting.name | decamel }}
      </label>
      <input
        :name="setting.name"
        type="checkbox"
        @input="input"
        :checked="setting.value"
      />
      {{ setting.comment }}
    </div>
  `,
  props: ['setting'],
  methods: {
    input(evt) {
      this.$emit('input', evt.target.checked);
    }
  }
};
