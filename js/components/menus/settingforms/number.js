const html = arg => arg.join('');

export default {
  template: html`
    <div class="form-group number">
      <label :for="setting.name">
        {{ setting.name | decamel }}
      </label>
      <input
        :name="setting.name"
        :type="(min == 0 && max == 1) ? 'range' : 'number'"
        :min="min"
        :max="max"
        @change="input"
        :value="setting.value"
        step="any"
      />
      {{ setting.comment }}
    </div>
  `,
  props: ['setting'],
  computed: {
    max() {
      if (typeof this.setting.max == 'undefined') return Infinity;
      return this.setting.max;
    },
    min() {
      if (typeof this.setting.min == 'undefined') return -Infinity;
      return this.setting.min;
    }
  },
  methods: {
    input(evt) {
      this.$emit('input', evt.target.value);
    }
  }
};
