const html = arg => arg.join('');

import Number from './settingforms/number.js';
import Boolean from './settingforms/boolean.js';
import Enum from './settingforms/enum.js';

export default {
  template: html`
    <div>
      <button class="gamebutton small right" @click="back">
        🡄 back
      </button>
      <div class="settings">
        <component
          v-for="setting of settingslist"
          :is="setting.type"
          :setting="setting"
          @input="input(setting, $event)"
        ></component>
      </div>
    </div>
  `,
  props: ['settings'],
  components: { Number, Boolean, Enum },
  data: () => ({ settingslist: [] }),
  mounted() {
    this.settingslist = [...this.settings.all()].sort((a,b) => {
      if (a.type > b.type) return -1;
      if (a.type < b.type) return 1;
      if (a.name > b.name) return 1;
      if (a.name < b.name) return -1;
      return 0;
    });
  },
  methods: {
    back() {
      this.$emit('navigate', 'mainmenu');
    },
    input(setting, val) {
      if (!this.settings.set(setting.name, val))
        console.warn(`Invalid value for ${setting.name}:`, val, `(${typeof val})`);
      this.settings.save();
      console.log('save');
    }
  }
}
