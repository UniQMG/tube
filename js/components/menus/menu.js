const html = arg => arg.join(''); // NOOP, for editor integration.

export default {
  template: html`
    <div>
      <h1>Tube (name subject to change)</h1>
      <div class="game-options-ct">
        <div class="game-options">
          <button @click="showMaps = !showMaps" class="gamebutton">
            Play
          </button>
          <button @click="open('editor')" class="gamebutton">
            Editor
          </button>
          <button @click="open('settings')" class="gamebutton">
            Settings
          </button>
          <button @click="open('about')" class="gamebutton">
            About
          </button>
        </div>
        <div class="game-options" v-if="showMaps">
          <button @click="setmap(map)" v-for="map of maps" class="gamebutton map">
            {{ map.name }}
          </button>
        </div>
      </div>
    </div>
  `,
  props: ['game'],
  data: () => ({
    maps: [],
    showMaps: false
  }),
  async mounted() {
    this.maps = await (await fetch('maps/maps.json')).json();
  },
  methods: {
    async setmap(map) {
      let mapdata = await import('../../../' + map.path);
      this.game.setMapData(mapdata.default);
      this.game.restart();
      this.open('game');
    },
    open(page) {
      this.$emit('navigate', page);
    }
  }
};
