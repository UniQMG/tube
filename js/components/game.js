import Editor from './editor/editor.js';
import keys from '../keys.js';

const html = arg => arg.join('');

export default {
  template: html`
    <div>
      <editor
        class="editor-container"
        v-if="showEditor"
        :game="getGame()"
        @close="closeEditor"
      />

      <div class="scoretext">
        {{ ~~progress }}m | {{ ~~score }}pts | {{ ~~speed }}m/s
      </div>

      <div
        v-for="text of texts"
        :key="text.key"
        :style="textStyle(text)"
        :class="text.class"
      >
        {{ text.text }}
      </div>

      <template v-if="!showEditor">
        <button class="gamebutton small right" @click="back">
          🡄 back
        </button>
        <button class="gamebutton small right" @click="showEditor = true">
          ✎ Editor
        </button>
      </template>
    </div>
  `,
  props: ['game', 'externalEditor'],
  components: { Editor },
  data: () => ({
    showEditor: false,
    score: 0,
    progress: 0,
    speed: 0,
    texts: [],
    inputs: [],
    map: null,
    maps: [],
    onDestroy: []
  }),
  async mounted() {
    this.maps = await (await fetch('maps/maps.json')).json();
    if (window.location.hash == '#dev')
      this.map = 'Distrion & Electro-Light - Rubik';

    let cb = this.game.on('frame', () => {
      this.progress = this.game.progress;
      this.score = this.game.score;
      this.speed = this.game.speed;
    });
    this.onDestroy.push(() => this.game.off('frame', cb));

    let keyUpd = setInterval(() => {
      this.inputs.length = 0;
      for (let entry of keys.axes())
        this.inputs.push(entry);
    }, 100);
    this.onDestroy.push(() => clearInterval(keyUpd));

    if (this.externalEditor)
      this.showEditor = true;
  },
  beforeDestroy() {
    for (let fn of this.onDestroy) fn();
  },
  watch: {
    async map(mapname) {
      let data = this.maps.filter(({ name }) => name == mapname)[0];
      let map = await import('../../' + data.path);
      this.game.setMapData(map.default);
      this.game.restart();
    }
  },
  methods: {
    back() {
      this.$emit('navigate', 'mainmenu');
      this.game.stop();
    },
    closeEditor() {
      if (this.externalEditor) {
        this.$emit('navigate', 'mainmenu');
      } else {
        this.showEditor = false;
      }
    },
    getGame() {
      return this.game;
    },
    textStyle(text) {
      return {
        position: 'fixed',
        top: text.y + 'px',
        left: text.x + 'px'
      }
    },
    createFloatingText(text, x, y, duration=3000, clazz=[]) {
      let obj = { key: Date.now(), text, x, y, class: clazz };
      this.texts.push(obj);
      setTimeout(() => {
        this.texts.splice(this.texts.indexOf(obj), 1)
      }, duration);
    }
  }
}
