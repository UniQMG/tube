const html = arg => arg.join(''); // NOOP, for editor integration.

const handlers = new Map();
const eval_global = {};
handlers.set('script', (env) => {
  const {
    key, event, game, objects, selected, app, editor, timeline, palette
  } = env;
  (function() { eval(env.key.script); }).call(eval_global);
});

const defaults = [];
const hotkeypromise = Promise.all([
  import('./hotkeys/edit/copy.js'),
  import('./hotkeys/edit/delete.js'),
  import('./hotkeys/edit/paste.js'),
  import('./hotkeys/edit/translate-down.js'),
  import('./hotkeys/edit/translate-left.js'),
  import('./hotkeys/edit/translate-right.js'),
  import('./hotkeys/edit/translate-up.js'),
  import('./hotkeys/marking/clear-marks.js'),
  import('./hotkeys/marking/jump-next-mark.js'),
  import('./hotkeys/marking/jump-prev-mark.js'),
  import('./hotkeys/marking/mark.js'),
  import('./hotkeys/marking/unmark.js'),
  import('./hotkeys/select/deselect.js'),
  import('./hotkeys/select/select-next.js'),
  import('./hotkeys/select/select-prev.js'),
  import('./hotkeys/special/sidebar.js'),
  import('./hotkeys/special/tool.js'),
  import('./hotkeys/pause.js'),
  import('./hotkeys/play-at-selected.js'),
  import('./hotkeys/redo.js'),
  import('./hotkeys/undo.js')
]).then(res => {
  for (let entry of res.flatMap(e => e.default)) {
    handlers.set(entry.name, entry.handle);
    if (Array.isArray(entry.default))
      for (let key of entry.default)
        defaults.push(key);
    else defaults.push(entry.default);
  }
});

export default {
  template: html`
    <div>
      Hotkeys
      <button @click="addHotkey">+Add</button>
      <button @click="addDefault">Add default</button>
      <button @click="save">Save all</button>
      <div class="hotkey" v-for="(key, i) of hotkeys">
        Combo:
        <input @keydown="rebind(key, $event)" :value="keyValue(key)" />
        <button @click="hotkeys.splice(i, 1)">delete</button>
        <br>

        Action:
        <select v-model="key.action">
          <option :value="action" v-for="action of actions">{{ action }}</option>
        </select><br>

        <textarea
          v-if="key.action == 'script'"
          @keydown.stop
          v-model="key.script"
          cols="40"
          rows="4"
        ></textarea>
      </div>
    </div>
  `,
  data: () => ({
    hotkeys: [],
    actions: [],
    handlers: {}
  }),
  mounted() {
    this.listener = this.listener.bind(this);
    document.addEventListener('keydown', this.listener);
    hotkeypromise.then(() => {
      this.actions = [...handlers.keys(), 'script'];
      this.handlers = Object.fromEntries(handlers.entries());
      this.addDefault();
      try {
        this.hotkeys = JSON.parse(localStorage.hotkeys);
      } catch(ex) {}
    });
  },
  beforeDestroy() {
    document.removeEventListener('keydown', this.listener);
    this.save();
  },
  methods: {
    save() {
      localStorage.hotkeys = JSON.stringify(this.hotkeys);
    },
    listener(evt) {
      function check(funcOrVal, val) {
        if (typeof funcOrVal == 'function')
          return funcOrVal(val);
        return funcOrVal == val;
      }
      for (let key of this.hotkeys) {
        if (!check(key.key, evt.key)) continue;
        if (!check(key.ctrl, evt.ctrlKey)) continue;
        if (!check(key.shift, evt.shiftKey)) continue;
        if (!check(key.alt, evt.altKey)) continue;
        this.$emit('hotkey', key, evt, handlers.get(key.action));
      }
    },
    rebind(key, evt) {
      evt.preventDefault();
      evt.stopPropagation();
      key.ctrl = evt.ctrlKey
      key.shift = evt.shiftKey;
      key.alt = evt.altKey;
      key.key = evt.key;
    },
    keyValue(key) {
      let mods = [];
      if (key.ctrl) mods.push('ctrl');
      if (key.shift) mods.push('shift');
      if (key.alt) mods.push('alt');
      switch (key.key) {
        case ' ':
          mods.push('Space');
          break;
        default:
          mods.push(key.key);
          break;
      }
      return mods.join('-');
    },
    addDefault() {
      this.hotkeys.push(...JSON.parse(JSON.stringify(defaults)));
    },
    addHotkey() {
      this.hotkeys.push({
        ctrl: false,
        shift: false,
        alt: false,
        key: null,
        action: 'script',
        script:
          `// Write some javascript! Don't paste untrusted code.\n` +
          `// Globals: 'game', 'objects', 'event', 'selected'\n` +
          `// Components: 'app', 'editor', 'timeline'\n` +
          `// This is eval()'d directly, so don't infinite loop yourself.\n` +
          `// Call event.preventDefault() yourself`
      })
    }
  }
}
