const html = arg => arg.join(''); // NOOP, for editor integration.

export default {
  template: html`
    <!-- <div :style="debugstyle">
      {{ debugtext }}
    </div> -->
  `,
  data: () => ({
    ctx: new AudioContext(),
    ctxTime: 0, // The current time of the audio ctx
    bufferSource: null, // The current playing buffer source
    isReversed: false, // Whether currently playing backwards
    startTime: 0, // The audio context time when the playbackRate was last changed
    elapsed: 0, // The total elapsed time at the last playbackRate change
  }),
  props: ['audiobuffer', 'playbackRate', 'paused'],
  mounted() {
    this.updateTime = this.updateTime.bind(this);
    this.updateTime();
  },
  computed: {
    playing() {
      return this.bufferSource && !this.paused;
    },
    debugstyle() {
      return {
        position: 'fixed',
        background: 'black',
        zIndex: 9999999,
        marginLeft: '50vw',
        marginTop: '50vh',
        padding: '10px 8px',
        transform: 'translateX(-50%) translateY(-50%)'
      }
    },
    debugtext() {
      return [
        `Rate: ${this.playbackRate.toFixed(2)}`,
        `Start: ${this.startTime.toFixed(2)}`,
        `Now: ${this.ctxTime.toFixed(2)}`,
        `Elapsed: ${this.elapsed.toFixed(2)}`,
        `Total: ${this.currentTime.toFixed(2)}`
      ].join(' | ')
    },
    currentTime() {
      return (this.ctxTime - this.startTime) * this.playbackRate + this.elapsed;
    }
  },
  watch: {
    paused(val) {
      (val ? this.ctx.suspend() : this.ctx.resume()).then(() => {
        this.$emit('timeupdate')
      });
    },
    playbackRate(val, old) {
      if (!this.bufferSource) return;
      this.elapsed += (this.ctx.currentTime - this.startTime) * old;
      this.startTime = this.ctx.currentTime;
      this.bufferSource.playbackRate.value = Math.abs(val);
    },
    currentTime() {
      this.$emit('timeupdate');
    }
  },
  methods: {
    updateTime() {
      this.ctxTime = this.ctx.currentTime;
      requestAnimationFrame(this.updateTime);
    },
    stop() {
      if (this.bufferSource) {
        this.bufferSource.stop();
        this.bufferSource = null;
      }
    },
    play(relStartTime) {
      this.stop();
      this.bufferSource = this.ctx.createBufferSource();
      this.bufferSource.buffer = this.audiobuffer;
      this.bufferSource.connect(this.ctx.destination);
      this.bufferSource.start(0, relStartTime);
      this.bufferSource.playbackRate.value = this.playbackRate;
      this.startTime = this.ctx.currentTime;
      this.elapsed = relStartTime;
    }
  }
}
