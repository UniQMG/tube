const html = arg => arg.join(''); // NOOP, for editor integration.

export default {
  template: html`
    <div class="palette">
      <span v-for="itool of tools">
        <button
          @click="tool = itool"
          :disabled="tool == itool"
        >{{ itool.name }}</button>
      </span>
      <br>
      <div>
        Clipboard: {{ clipboard.length }} objects<br>
        <div v-for="slice of reverseHistory">
          {{ slice.time }} |
          {{ slice.objects.length }} objects
          <button @click="copy(slice.objects, false)">Load</button>
          <button @click="remove(slice)">Delete</button>
        </div>
      </div>
      <br>
      <div>
        Undo history: {{ undoStates.length }}/1000 items.
        <button @click="trimHistory">trim</button>
        <div v-for="undo of undoStates">
          Undo: {{ undo.name }} @ {{ undo.time }}
        </div>
        <div>
          -- Current state --
        </div>
        <div v-for="redo of redoStates">
          Redo: {{ redo.name }} @ {{ redo.time }}
        </div>
      </div>
    </div>
  `,
  props: ['undoStates', 'redoStates'],
  data: () => ({
    tool: null,
    clipboard: [],
    history: [],
    tools: [{
      name: 'obstacle',
      region: (editor, { distance, x_min, x_max, y_min, y_max }) => {
        if (distance < 35) return;
        editor.pushUndo('create obstacle');
        let obj = {
          type: 'obstacle',
          progress: y_min * editor.timestep * 1/5,
          length: (y_max - y_min) * editor.timestep * 1/5,
          start: x_min,
          end: x_max
        };
        editor.objects.push(obj);
        editor.addObject(obj);
        editor.timeline.clearSelection();
        editor.selected.push(obj);
      }
    }, {
      name: 'orb',
      region: (editor, { x_min, x_max, y_min, y_max }) => {
        editor.pushUndo('create orb');
        let ang = (x_min + x_max)/2;
        let obj = {
          type: 'orb',
          progress: (y_max + y_min)/2 * editor.timestep * 1/5,
          length: 1,
          start: ang - 5,
          end: ang + 5
        };
        editor.objects.push(obj);
        editor.addObject(obj);
        editor.timeline.clearSelection();
        editor.selected.push(obj);
      }
    }, {
      name: 'light',
      region: (editor, { y_min, y_max }) => {
        editor.pushUndo('create light');
        let obj = {
          type: 'light',
          progress: (y_min + y_max)/2 * editor.timestep * 1/5,
          length: 1,
          start: 0,
          end: 0,
          light: {
            color: 0x00FF00,
            intensity: 1,
            distance: 100
          }
        };
        editor.objects.push(obj);
        editor.addObject(obj);
        editor.timeline.clearSelection();
        editor.selected.push(obj);
      }
    }, {
      name: 'keyframe',
      region: (editor, { y_min, y_max }) => {
        editor.pushUndo('create keyframe');
        let obj = {
          type: 'keyframe',
          progress: (y_min + y_max)/2 * editor.timestep * 1/5,
          length: 1,
          start: 0,
          end: 0,
          keyframe: {
            duration: 1,
            intensity: 1,
            peakIntensity: 2,
            color: 0xFFFFFF,
            effect: 'on',
            light: 'main'
          }
        }
        editor.objects.push(obj);
        editor.addObject(obj);
        editor.timeline.clearSelection();
        editor.selected.push(obj);
      }
    }]
  }),
  methods: {
    copy(objects) {
      this.clipboard.length = 0;
      this.clipboard.push(...objects);
      this.history.push({
        time: /\d+:\d+:\d+/.exec(new Date())[0],
        objects: [...objects]
      });
    },
    remove(slice) {
      this.history.splice(this.history.indexOf(slice), 1);
    },
    trimHistory() {
      this.undoStates.splice(0);
      this.redoStates.splice(0);
    }
  },
  computed: {
    reverseHistory() {
      return this.history.slice().reverse();
    },
    reverseUndoes() {
      return this.undoStates.slice().reverse();
    },
    reverseRedoes() {
      return this.redoStates.slice().reverse();
    }
  },
  mounted() {
    this.tool = this.tools[0];
  }
}
