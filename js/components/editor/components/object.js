const html = arg => arg.join(''); // NOOP, for editor integration.

export default {
  template: html`
    <div>
      <div
        class="timeline-object"
        :index="index"
        :class="{ 'timeline-object': true, selected, [object.type]: true }"
        :style="{
          '--time': object.progress,
          '--length': object.length,
          '--start': ghostable ? Math.max(object.start, 0) : object.start,
          '--end': ghostable ? Math.min(object.end, 360) : object.end,
          ...extrastyles
        }"
      >
        <template v-if="resizable">
          <div class="handle handle-side handle-top"></div>
          <div class="handle handle-side handle-left"></div>
          <div class="handle handle-side handle-right"></div>
          <div class="handle handle-side handle-bottom"></div>
          <div class="handle handle-top handle-left"></div>
          <div class="handle handle-top handle-right"></div>
          <div class="handle handle-bottom handle-left"></div>
          <div class="handle handle-bottom handle-right"></div>
        </template>
      </div>
      <div
        class="timeline-object ghost"
        v-if="object.end > 360 && ghostable"
        :index="index"
        :objtype="object.type"
        :style="{
          '--time': object.progress,
          '--length': object.length,
          '--start': 0,
          '--end': object.end-360
        }"
      />
      <div
        class="timeline-object ghost"
        v-if="object.start < 0 && ghostable"
        :index="index"
        :objtype="object.type"
        :style="{
          '--time': object.progress,
          '--length': object.length,
          '--start': 360 + object.start,
          '--end': 360
        }"
      />
    </div>
  `,

  props: ['object', 'index', 'selected', 'timestep', 'extrastyles'],
  computed: {
    resizable() {
      switch (this.object.type) {
        case 'obstacle': return true;
        default: return false;
      }
    },
    ghostable() {
      switch (this.object.type) {
        case 'obstacle': return true;
        default: return false;
      }
    }
  },
  watch: {
    object: {
      handler() {
        this.$emit('update');
      },
      deep: true
    }
  }
}
