export default {
  name: 'paste',
  handle({ event, timeline, palette }) {
    let prog = timeline.cursorProgress;
    timeline.clearSelection();
    timeline.selected.push(...timeline.cloneMultiple(palette.clipboard, prog));
  },
  get default() {
    return {
      undoable: true,
      ctrl: true,
      alt: false,
      shift: false,
      key: 'v',
      action: 'paste',
    }
  }
}
