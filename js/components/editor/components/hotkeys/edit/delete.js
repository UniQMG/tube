import selectnext from '../select/select-next.js';

export default {
  name: 'delete',
  handle({ vent, editor, timeline }) {
    let selected = timeline.selected.slice();
    // Select next object before removing current
    selectnext.handle({ event, timeline });
    for (let obj of selected)
      timeline.removeObject(obj);
  },
  get default() {
    return {
      undoable: true,
      ctrl: false,
      alt: false,
      shift: false,
      key: 'Delete',
      action: 'delete',
    }
  }
}
