export default {
  name: 'translate-left',
  handle({ event, editor, timeline }) {
    if (!timeline.selected.length) return;
    event.preventDefault();

    for (let selected of timeline.selected) {
      if (event.ctrlKey) {
        selected.end -= 5;
        break;
      }
      selected.start -= 5;
      if (!event.shiftKey)
        selected.end -= 5;
    }

    timeline.enforceLimit();
  },
  get default() {
    return {
      ctrl: () => true,
      alt: false,
      shift: () => true,
      key: 'ArrowLeft',
      action: 'translate-left',
    }
  }
}
