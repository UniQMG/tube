export default {
  name: 'translate-down',
  handle({ event, editor, timeline }) {
    if (!timeline.selected.length) return;
    event.preventDefault();

    for (let selected of timeline.selected) {
      if (event.shiftKey) {
        selected.length += this.timestep;
      } else if(event.ctrlKey) {
        selected.progress += this.timestep;
        selected.length -= this.timestep;
      } else {
        selected.progress += this.timestep;
      }
    }

    timeline.enforceLimit();
  },
  get default() {
    return {
      ctrl: () => true,
      alt: false,
      shift: () => true,
      key: 'ArrowDown',
      action: 'translate-down',
    }
  }
}
