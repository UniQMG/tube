export default {
  name: 'translate-right',
  handle({ event, editor, timeline }) {
    if (!timeline.selected.length) return;
    event.preventDefault();

    for (let selected of this.selected) {
      if (event.ctrlKey) {
        selected.start += 5;
        break;
      }
      if (!event.shiftKey)
        selected.start += 5;
      selected.end += 5;
    }

    timeline.enforceLimit();
  },
  get default() {
    return {
      ctrl: () => true,
      alt: false,
      shift: () => true,
      key: 'ArrowRight',
      action: 'translate-right',
    }
  }
}
