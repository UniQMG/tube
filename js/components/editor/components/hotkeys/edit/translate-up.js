export default {
  name: 'translate-up',
  handle({ event, editor, timeline }) {
    if (!timeline.selected.length) return;
    event.preventDefault();

    for (let selected of this.selected) {
      if (event.shiftKey) {
        selected.progress -= this.timestep;
        selected.length += this.timestep;
      } else if (event.ctrlKey) {
        selected.length -= this.timestep;
      } else {
        selected.progress -= this.timestep;
      }
    }

    timeline.enforceLimit();
  },
  get default() {
    return {
      ctrl: () => true,
      alt: false,
      shift: () => true,
      key: 'ArrowUp',
      action: 'translate-up',
    }
  }
}
