export default {
  name: 'copy',
  handle({ event, timeline, palette }) {
    palette.copy(timeline.selected);
  },
  get default() {
    return {
      undoable: true,
      ctrl: true,
      alt: false,
      shift: false,
      key: 'c',
      action: 'copy',
    }
  }
}
