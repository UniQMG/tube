export default {
  name: 'redo',
  handle({ event, editor, timeline }) {
    event.preventDefault();
    editor.redo();
  },
  get default() {
    return [{
      ctrl: true,
      alt: false,
      shift: false,
      key: 'y',
      action: 'redo',
    }, {
      ctrl: true,
      alt: false,
      shift: true,
      key: 'Z',
      action: 'redo',
    }]
  }
}
