export default {
  name: 'jump-next-mark',
  handle({ event, editor, timeline }) {
    let foundMark = false;
    for (let amp of editor.waveform) {
      if (amp.time <= editor.currentTime) continue;
      if (amp.marked) {
        editor.jump(amp.time);
        return;
      }
    }
    editor.jump(editor.waveform[editor.waveform.length-1].time);
  },
  get default() {
    return {
      ctrl: false,
      alt: false,
      shift: false,
      key: ']',
      action: 'jump-next-mark',
    }
  }
}
