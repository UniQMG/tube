export default {
  name: 'unmark',
  handle({ event, editor, timeline }) {
    editor.getLastActiveWaveform().marked = false;
  },
  get default() {
    return {
      undoable: true,
      ctrl: false,
      alt: false,
      shift: false,
      key: '-',
      action: 'unmark',
    }
  }
}
