export default {
  name: 'jump-prev-mark',
  handle({ event, editor, timeline }) {
    let foundMark = false;
    for (let amp of [...editor.waveform].reverse()) {
      if (amp.time >= editor.currentTime) continue;
      if (amp.marked) {
        editor.jump(amp.time);
        return;
      }
    }
    editor.jump(0);
  },
  get default() {
    return {
      ctrl: false,
      alt: false,
      shift: false,
      key: '[',
      action: 'jump-prev-mark',
    }
  }
}
