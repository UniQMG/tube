export default {
  name: 'mark',
  handle({ event, editor, timeline }) {
    editor.getLastActiveWaveform().marked = true;
  },
  get default() {
    return {
      undoable: true,
      ctrl: false,
      alt: false,
      shift: false,
      key: '+',
      action: 'mark',
    }
  }
}
