export default {
  name: 'clear-marks',
  handle({ event, editor, timeline }) {
    event.preventDefault();
    for (let amp of editor.waveform)
      amp.marked = false;
  },
  get default() {
    return {
      undoable: true,
      ctrl: false,
      alt: false,
      shift: false,
      key: '/',
      action: 'clear-marks',
    }
  }
}
