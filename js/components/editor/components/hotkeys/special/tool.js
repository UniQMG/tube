import palette from '../../palette.js';

const tools = palette.data().tools.map(tool => tool.name);
export default tools.map((name, i) => ({
  name: 'tool-' + name,
  handle({ event, palette }) {
    palette.tool = palette.tools.filter(tool => tool.name == name)[0];
    console.log("Set");
  },
  get default() {
    return {
      ctrl: false,
      alt: false,
      shift: false,
      key: i+1,
      action: 'tool-' + name,
    }
  }
}));
