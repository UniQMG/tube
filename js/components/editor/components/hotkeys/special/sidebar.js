export default [null, 'hotkey-manager', 'palette'].map((name, i) => ({
  name: 'sidebar-pane-' + (name || 'collapsed'),
  handle({ event, editor }) {
    event.preventDefault();
    editor.sidebar = name
  },
  get default() {
    return {
      ctrl: true,
      alt: false,
      shift: false,
      key: i+1,
      action: 'sidebar-pane-' + (name || 'collapsed'),
    }
  }
}));
