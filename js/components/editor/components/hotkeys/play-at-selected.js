export default {
  name: 'play-at-selected',
  handle({ event, editor, timeline }) {
    event.preventDefault();
    if (!timeline.selected.length) return;
    editor.play(timeline.selected[0].progress);
  },
  get default() {
    return {
      ctrl: false,
      alt: false,
      shift: true,
      key: ' ',
      action: 'play-at-selected',
    }
  }
}
