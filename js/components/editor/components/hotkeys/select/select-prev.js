// Todo: find some non-ridiculous way to merge this with select-next
export default {
  handle({ event, timeline }) {
    event.preventDefault();
    let selected = timeline.selected[0];
    if (!timeline.selected) {
      timeline.selected.length.splice(0);
      timeline.selected.push(timeline.objects[0]);
      return;
    }
    let sorted = timeline.objects.slice().sort((a,b) => {
      if (a.progress == b.progress)
        return a.start > b.start ? 1 : -1;
      return a.progress > b.progress ? 1 : -1
    });
    let si = sorted.indexOf(selected);
    si = (si + -1 + sorted.length) % sorted.length;
    timeline.selected.length.splice(0);
    timeline.selected.push(sorted[si]);
  },
  get name() {
    return 'select-prev'
  },
  get default() {
    return {
      undoable: true,
      ctrl: false,
      alt: false,
      shift: true,
      key: 'Tab',
      action: 'select-prev',
    }
  }
}
