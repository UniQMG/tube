export default {
  name: 'deselect',
  handle({ event, editor, timeline }) {
    timeline.clearSelection();
  },
  get default() {
    return [{
      undoable: true,
      ctrl: false,
      alt: false,
      shift: false,
      key: 'Escape',
      action: 'deselect',
    }, {
      undoable: true,
      ctrl: true,
      alt: false,
      shift: true,
      key: 'A',
      action: 'deselect',
    }]
  }
}
