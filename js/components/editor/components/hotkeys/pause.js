export default {
  name: 'pause',
  handle({ event, editor, timeline }) {
    event.preventDefault();
    editor.togglepause();
  },
  get default() {
    return {
      ctrl: false,
      alt: false,
      shift: false,
      key: ' ',
      action: 'pause',
    }
  }
}
