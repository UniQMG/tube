export default {
  name: 'undo',
  handle({ event, editor, timeline }) {
    event.preventDefault();
    editor.undo();
  },
  get default() {
    return {
      ctrl: true,
      alt: false,
      shift: false,
      key: 'z',
      action: 'undo',
    }
  }
}
