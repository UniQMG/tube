const html = arg => arg.join(''); // NOOP, for editor integration.

/*
  The canvas has a container because the glslcanvas.js library
  automatically resizes the canvas to its actual size, which is
  not what we want since we run into canvas size limitations on
  longer songs. This container has a fixed, calculated height
  and the canvas is resized using transform: scale()
  Note that firefox's max canvas size is 32767 pixels which is
  about 13 minutes 39 seconds at the default res of 0.025
*/

export default {
  template: html`
    <div class="waveform" ref="waveform">
      <div class="canvas-container" :style="ctStyle">
        <canvas
          ref="canvas"
          width="80"
          :style="canvasStyle"
          :height="scaledItemSize * waveform.length"
          data-fragment-url="res/shader/waveform.frag"
          @click="click"
        ></canvas>
      </div>
    </div>
  `,
  props: ['waveform', 'currentTime', 'autoscroll', 'timestep', 'zoom'],
  mounted() {
    window.wv = this;
    this.sandbox = new GlslCanvas(this.$refs.canvas);
    this.sandbox.loadTexture("u_waveform", this.dataCanvas);
  },
  data: () => ({
    sandbox: null,
    dataCanvas: document.createElement('canvas'),
    waveformWatchers: [],
  }),
  methods: {
    click(evt) {
      let { top } = this.$refs.canvas.getBoundingClientRect();
      let y = evt.clientY - top;
      let time = y / this.itemSize * this.timestep;
      this.$emit('scrub', time);
    },
    refreshUniforms() {
      this.sandbox.setUniform("u_current_time", this.currentTime);
      this.sandbox.setUniform("u_timestep", this.timestep);
      this.sandbox.setUniform("u_item_size", this.scaledItemSize);
      this.sandbox.setUniform("u_wavecount", this.waveform.length);
    },
    draw(ctx, amp, i) {
      let data = ctx.createImageData(1, 1);
      // Waveform data, we don't really need more than 6 bits because the
      // output is only 40px (80/2) wide.
      data.data[0] = ~~(256 * amp.rms);
      // Marked data. Only needs a bit. We have plenty of free space thouhg
      data.data[1] = ~~(1 & amp.marked);
      // Nothing more to pack, could pack previous stuff way better though
      data.data[2] = 0;
      data.data[3] = 255;
      ctx.putImageData(data, i, 0);
    }
  },
  computed: {
    scaleFactor() {
      return this.zoom * 5;
    },
    itemSize() {
      return this.zoom * 5;
    },
    scaledItemSize() {
      return this.itemSize / this.scaleFactor; // Yes, its always 1
    },
    ctStyle() {
      return { width: '80px', height: this.itemSize * this.waveform.length + 'px' };
    },
    canvasStyle() {
      return {
        imageRendering: 'crisp-edges',
        transform: `scaleY(${this.scaleFactor})`,
        transformOrigin: 'top'
      }
    }
  },
  watch: {
    waveform: {
      handler() {
        this.waveformWatchers.splice(0).forEach(unwatch => unwatch());

        console.log("Performing full redraw");
        this.sandbox.setUniform("u_waves", this.waveform.length);

        let ctx = this.dataCanvas.getContext('2d');
        this.dataCanvas.width = this.waveform.length;
        this.dataCanvas.height = 1;

        this.waveform.forEach((amp, i) => {
          this.draw(ctx, amp, i);
          this.waveformWatchers.push(this.$watch(function() {
            return amp.rms + amp.marked;
          }, function() {
            console.log("Watched!");
            this.draw(ctx, amp, i);
            this.sandbox.textures.u_waveform.update();
          }));
        });

        this.sandbox.textures.u_waveform.update();
        this.refreshUniforms();
      },
      initial: true
    },
    timestep: {
      handler() {
        this.sandbox.setUniform("u_timestep", this.timestep);
      },
      deep: true,
      initial: true
    },
    waveformTexture: {
      handler() {
        // Why do we have to reload all of them at this point?
        this.refreshUniforms();
      },
      deep: true,
      initial: true
    },
    scaledItemSize: {
      handler() {
      this.sandbox.setUniform("u_item_size", this.scaledItemSize);
      },
      deep: true,
      initial: true
    },
    currentTime: {
      handler() {
        this.sandbox.setUniform("u_current_time", this.currentTime)
        if (this.autoscroll) {
          let y = this.currentTime / this.timestep * this.itemSize;
          let height = document.querySelector('.header').clientHeight + 8;
          y -= (window.innerHeight - height) / 2 - height;
          window.scrollTo(0, y);
        }
      },
      deep: true,
      initial: true
    }
  }
}
