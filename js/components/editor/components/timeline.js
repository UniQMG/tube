import interacthandler from '../interacthandler.js';
import TimelineObject from './object.js';
import DragSelect from '../../../lib/ds.es6m.min.js';
const html = arg => arg.join(''); // NOOP, for editor integration.

const id = Symbol.for('timeline-object-id');
const idmap = new WeakMap();

export default {
  template: html`
    <div class="timeline" ref="timeline" @mousemove="mousemove" @contextmenu="$event.preventDefault()">
      <timeline-object
        v-for="(object, i) of objects"
        @update="updateObject(object)"
        :extrastyles="extrastyles(object)"
        :timestep="timestep"
        :key="objectuid(object)"
        :selected="selected.indexOf(object) !== -1"
        :object="object"
        :index="i"
      />
      <div class="object-editor" v-if="selected.length">
        <template v-for="select of selected">
          Progress (x, seconds)
          <input type="number" v-model.number="select.progress" min="0" :step="timestep" />

          Length (x, seconds / speed)
          <input type="number" v-model.number="select.length" min="0.1" :step="timestep" />

          Start (y, degrees)
          <input type="number" v-model.number="select.start" step="1" />

          End (y, degrees)
          <input type="number" v-model.number="select.end" step="1" />

          <template v-if="select.type == 'light'">
            Color
            <input type="color" v-model="select.light.color"/>
            Intensity
            <input type="number" v-model.number="select.light.intensity" step="0.1" />
            Distance
            <input type="number" v-model.number="select.light.distance" />
          </template>

          <template v-if="select.type == 'keyframe'">
            Light <select v-model="select.keyframe.light">
              <option value="main">Main</option>
            </select>
            Effect <select v-model="select.keyframe.effect">
              <option value="on">Turn on</option>
              <option value="off">Turn off</option>
              <option value="fade">Fade</option>
            </select>
            <template v-if="select.keyframe.effect != 'off'">
              Intensity
              <input type="number" v-model.number="select.keyframe.intensity" step="0.1" />
              Color <input
                type="color"
                :value="'#' + select.keyframe.color.toString(16)"
                @input="select.keyframe.color = parseInt($event.target.value.slice(1), 16)"
              />
            </template>
            <template v-if="select.keyframe.effect == 'fade'">
              Duration (seconds/speed)
              <input type="number" v-model.number="select.keyframe.duration" />
              Starting intensity
              <input type="number" v-model.number="select.keyframe.peakIntensity" />
            </template>
          </template>

          <div>
            <button @click="clone(select)">Clone</button>
            <button @click="removeObject(select)">Delete</button>
          </div>
          <div><!-- spacer --></div>
        </template>
      </div>
    </div>
  `,
  components: { TimelineObject },
  props: ['objects', 'timestep', 'zoom'],
  data: () => ({
    cursorProgress: 0,
    selected: [],
    dragselect: null,
    id: id
  }),
  mounted() {
    this.interactable = interacthandler(
      this,
      () => this.timestep,
      () => this.zoom,
      type => this.$emit('pushUndo', type + ' object')
    );
    this.dragselect = new DragSelect({
      area: this.$refs.timeline,
      callback: this.ondragselect.bind(this)
    });
    // https://github.com/ThibaultJanBeyer/DragSelect/blob/master/src/DragSelect.js#L1021
    // Overriden since we want *only* rightclicks. Returning true from this
    // indicates that the selection *shouldn't* do anything.
    this.dragselect._isRightClick = event => {
      return false;
    }
    window.ds = this.dragselect;
  },
  beforeUnmount() {
    this.interactable.unset();
    this.dragselect.stop();
  },
  watch: {
    objects: {
      immediate: true,
      handler() {
        this.$nextTick(() => this.populateDragSelect());
      }
    }
  },
  methods: {
    objectuid(object) {
      if (!idmap.has(object))
        idmap.set(object, Symbol());
      return idmap.get(object);
    },
    extrastyles(object) {
      switch (object.type) {
        case 'light': return {
          '--color': '#' + ('000000' + object.light.color.toString(16)).slice(-6)
        };
        case 'keyframe': return {
          '--duration': object.keyframe.duration,
          '--light': '#' + object.keyframe.color.toFixed(16)
        }
        default: return {};
      }
    },
    ondragselect(elems, evt) {
      let objects = elems
        .map(el => this.objects[el.getAttribute('index')])
        .filter(el => el);
      let start = this.dragselect.getInitialCursorPosition();
      let end = this.dragselect.getCurrentCursorPosition();
      let distance = Math.sqrt((start.x - end.x)**2 + (start.y - end.y)**2);

      let node = evt.target;
      while (node != this.$refs.timeline) {
        node = node.parentNode;
        if (!node || node.classList.contains('object-editor'))
          return false;
      }

      if (evt.button == '2' || (distance < 10 && elems.length > 0)) {
        this.clearSelection();
        this.selected.push(...objects);
      } else {
        this.$emit('region', {
          dragselect: this.dragselect,
          x_min: Math.min(start.x, end.x),
          x_max: Math.max(start.x, end.x),
          y_min: Math.min(start.y, end.y) / this.zoom,
          y_max: Math.max(start.y, end.y) / this.zoom,
          distance,
          objects
        });
      }
    },
    populateDragSelect() {
      let query = this.$refs.timeline.querySelectorAll('.timeline-object');
      this.dragselect.setSelectables(query);
    },
    clearSelection() {
      this.dragselect.clearSelection();
      return this.selected.splice(0);
    },
    updateObject(object) {
      this.$emit('updateObject', object);
    },
    addObject(object) {
      object[id] = object[id] || (Date.now() + '-' + ~~(Math.random() * 100000));
      this.$emit('addObject', object);
      this.objects.push(object);
    },
    removeObject(object) {
      let index = this.objects.indexOf(object);
      this.$emit('removeObject', object);
      this.objects.splice(index, 1);
    },
    clone(obj) {
      let swp = this.selected;
      this.selected = [obj];
      this.cloneSelected();
      this.selected = swp;
    },
    cloneSelected() {
      let min = this.selected.reduce(
        (min, val) => Math.min(min, val.progress),
        Infinity
      );
      let max = this.selected.reduce(
        (max, val) => Math.max(max, val.progress + val.length),
        0
      );
      this.selected.push(...this.cloneMultiple(this.clearSelection()));
    },
    cloneMultiple(objects, progress) {
      let minprog = objects.reduce((min, obj) => Math.min(min, obj.progress), Infinity);
      return objects.map(obj => {
        let copy = JSON.parse(JSON.stringify(obj));
        this.addObject(copy);
        copy.progress = (copy.progress - minprog) + progress;
        return copy;
      });
    },
    mousemove(evt) {
      let rect = this.$refs.timeline.getBoundingClientRect();
      let y = evt.clientY - rect.top
      let progress = y * this.timestep * 1/5;
      this.cursorProgress = progress / this.zoom;
    },
    enforceLimit() {
      for (let selected of this.selected) {
        selected.progress = Math.max(selected.progress, 0);
        selected.length = Math.max(selected.length, 0.01);
        selected.start = Math.max(selected.start, -180);
        selected.end = Math.min(selected.end, 360+180);
        selected.end = Math.max(selected.end, selected.start+1);
      }
    },
  }
}
