import AudioPlayer from './components/audioplayer.js';
import Waveform from './components/waveform.js';
import Timeline from './components/timeline.js';
import HotkeyManager from './components/hotkeymanager.js';
import Palette from './components/palette.js';
const html = arg => arg.join(''); // NOOP, for editor integration.
const decoder = new AudioContext();
const timestep = 0.025;

const proxy = Symbol();

export default {
  template: html`
    <div>
      <div
        class="decoding-notice"
        v-if="rendering"
      >Decoding audio. You may recieve unresponsive script prompts</div>
      <audio-player
        v-if="audio"
        ref="audioplayer"
        :audiobuffer="audio"
        :playback-rate="playbackRate"
        :paused="paused"
        @timeupdate="timeupdate"
      />
      <div class="header">
        <div>
          Tube Editor
          <button @click="close">Close editor</button>
          <button @click="save">Copy data to clipboard</button>
          <button @click="load">Load from string</button>
          <button @click="togglepause" title="hotkey: space">Pause</button>
          <br>

          Resolution:
          <input type="text" v-model.number="newTimestep" />
          <button @click="applyTimestep()">Apply</button>
          |
          Zoom
          <input type="number" v-model.number="zoom" step="0.05" />
          |
          Track:
          <input type="text" v-model="track" :class="{ notrack: !track }"/>
          <button @click="reanalyze">Fetch!</button>

          <div style="display: flex; align-items: center;">
            <input
              type="range"
              min="0.05"
              max="4"
              step="0.05"
              v-model.number="playbackRate"
            />
            Playback rate (x{{ playbackRate }}) |

            <input type="checkbox" v-model="followPlayback" />
            Auto-scroll with playback
            |
            Map speed
            <input type="number" v-model.number="mapSpeed" />
          </div>
        </div>

        <div class="shortcuts">
          Leftclick+drag: Draw objects (See palette tab)<br>
          Rightclick+drag: Select objects<br>
          See hotkeys tab for other actions
        </div>
      </div>

      <div class="editor" :style="{ '--timestep': timestep, '--ts-scale': zoom }">
        <waveform
          ref="waveform"
          :waveform="viewport"
          :timestep="timestep"
          :zoom="zoom"
          :current-time="currentTime"
          :autoscroll="followPlayback"
          @scrub="play"
        />

        <timeline
          ref="timeline"
          :objects="objects"
          :timestep="timestep"
          :zoom="zoom"
          @pushUndo="pushUndo"
          @region="toolregion"
          @addObject="addObject"
          @updateObject="updateObject"
          @removeObject="removeObject"
          @scrub="play"
        />

        <div class="canvas-container" ref="canvas" />

        <div class="sidebar">
          <div class="sidebar-content">
            <hotkey-manager v-show="sidebar == 'hotkey-manager'" @hotkey="hotkey"/>
            <palette
              ref="palette"
              v-show="sidebar == 'palette'"
              :undo-states="undoStates"
              :redo-states="redoStates"
            />
          </div>
          <div class="selectors">
            <button
              @click="sidebar = null"
              :disabled="sidebar == null"
            >Collapsed</button>
            <button
              @click="sidebar = 'hotkey-manager'"
              :disabled="sidebar == 'hotkey-manager'"
            >Hotkeys</button>
            <button
              @click="sidebar = 'palette'"
              :disabled="sidebar == 'palette'"
            >Palette</button>
          </div>
        </div>
      </div>

      <div class="loader" v-if="loading">
        <textarea v-model="loadString"></textarea>
        <button @click="loading = false">cancel</button>
        <button @click="confirmload">ok</button>
      </div>
    </div>
  `,
  props: ['game'],
  components: { AudioPlayer, Waveform, Timeline, HotkeyManager, Palette },
  data: () => ({
    sidebar: null,
    timestep,
    zoom: 2,
    newTimestep: timestep,
    followPlayback: false,
    currentTime: 0,
    playbackRate: 1.0,
    mapSpeed: 50,
    paused: false,
    loading: false,
    loadString: "",
    track: null,
    audio: null, // The loaded audio buffer
    objects: [],
    waveform: [],
    rendering: false,
    undoStates: [],
    redoStates: []
  }),
  mounted() {
    this.game.editing = true;
    window.editor = this;

    if (this.game.speed)
      this.mapSpeed = this.game.speed;
    if (this.game.track)
      this.track = this.game.track;
    if (this.game.mapdata) {
      this.objects.length = 0;
      this.objects.push(...this.game.mapdata.map(obj => {
        let clone = obj.isproxy ? obj.proxiedvalue : {...obj};
        if (!obj.isproxy) clone.progress /= this.mapSpeed;
        return clone;
      }));
    }

    if (this.track) this.reanalyze();
    this.update();

    this.$refs.canvas.appendChild(this.game.canvas);
    this.$nextTick(() => this.game.refit());
  },
  beforeDestroy() {
    this.game.editing = false;
    document.body.prepend(this.game.canvas);
    this.$nextTick(() => this.game.refit());
    if (this.audio)
      this.$refs.audioplayer.stop();
  },
  computed: {
    tool() {
      return this.$refs.palette.tool;
    },
    viewport() {
      if (this.rendering) return [];
      return this.waveform;
    },
    selected() {
      return this.$refs.timeline.selected;
    },
    timeline() {
      return this.$refs.timeline;
    },
    playing() {
      if (!this.audio) return false;
      return this.$refs.player?.playing;
    }
  },
  watch: {
    // objects: {
    //   handler() {
    //     this.game.refreshObjects();
    //   },
    //   deep: true
    // },
    sidebar() {
      this.$nextTick(() => this.game.refit());
    },
    mapSpeed() {
      this.update()
    }
  },
  methods: {
    pushUndo(reason) {
      // console.log('Pushed undo state', reason);
      this.undoStates.push({
        name: reason,
        time: /\d+:\d+:\d+/.exec(new Date())[0],
        objects: JSON.stringify(this.objects)
      });
      this.redoStates.splice(0);
      if (this.undoStates.length >= 1100) {
        this.undoStates = this.undoStates.slice(-1000);
      }
    },
    undo() {
      let state = this.undoStates.pop();
      if (!state) return;
      this.redoStates.push(state);
      this._applyUndoState(state);
    },
    redo() {
      let state = this.redoStates.pop();
      if (!state) return;
      this.undoStates.push(state);
      this._applyUndoState(state);
    },
    _applyUndoState(undo) {
      for (let obj of this.objects.splice(0))
        this.removeObject(obj);
      for (let obj of JSON.parse(undo.objects)) {
        this.objects.push(obj);
        this.addObject(obj);
      }
    },
    toolregion(region) {
      this.tool.region(this, region);
    },
    hotkey(key, evt, handler) {
      if (key.undoable) this.pushUndo(key.action.replace(/-/g, ' '));
      handler({
        key: key,
        event: evt,
        game: this.game,
        objects: this.objects,
        selected: this.$refs.timeline.selected,
        app: this.$root,
        editor: this,
        timeline: this.$refs.timeline,
        palette: this.$refs.palette
      })
    },
    addObject(obj) {
      // console.log("Add", obj[proxy]);
      this.ensureProxy(obj);
      game.addObject(obj[proxy]);
    },
    updateObject(obj) {
      this.ensureProxy(obj);
      game.updateObject(obj[proxy]);
    },
    removeObject(obj) {
      // console.log("Remove", obj[proxy]);
      this.ensureProxy(obj);
      game.deleteObject(obj[proxy]);
    },
    ensureProxy(obj) {
      const that = this;
      const workingcopy = {};
      if (!obj) return; // ???
      obj[proxy] = obj[proxy] || new Proxy(obj, {
        get: function(obj, prop) {
          switch (prop) {
            case 'isproxy':
              return true;

            case 'proxiedvalue':
              return obj;

            case 'progress':
              return obj[prop] * that.mapSpeed;

            default:
              return workingcopy[prop] || obj[prop];
          }
        },
        set: function(obj, prop, value) {
          workingcopy[prop] = value;
          return true;
        }
      });
    },
    timeupdate() {
      let player = this.$refs.audioplayer;
      this.currentTime = player.currentTime;
      this.game.paused = !player.playing;
      this.game.scrubTo(player.currentTime);
    },
    update() {
      this.objects.sort((a,b) => a.progress - b.progress);
      for (let obj of this.objects)
        this.ensureProxy(obj);
      const that = this;
      this.game.setMapData({
        track: this.track,
        speed: this.mapSpeed,
        mapdata: that.objects.map(obj => obj[proxy]),
        *generator() {
          for (let obj of that.objects) {
            that.ensureProxy(obj);
            yield obj[proxy];
          }
        }
      });
    },
    save() {
      let textArea = document.createElement("textarea");
      textArea.value = JSON.stringify({
        track: this.track,
        objects: this.objects
      });
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();
      document.execCommand('copy');
      textArea.remove();
    },
    load() {
      this.loading = true;
      this.loadString = "";
    },
    confirmload() {
      let { track, objects } = JSON.parse(this.loadString);
      this.track = track;
      this.objects = objects;
      this.loading = false;
    },
    getLastActiveWaveform() {
      let last = this.waveform[0];
      for (let amp of this.waveform) {
        if (this.currentTime > amp.time)
          last = amp;
        else break;
      }
      return last;
    },
    close() {
      this.game.paused = false;
      this.$emit('close');
    },
    togglepause() {
      this.paused = !this.paused;
    },
    normalize(dB) {
      let low = -60;
      let high = -10;
      return Math.max((dB - low)/(high - low), 0.05);
    },
    stop() {
      this.$refs.audioplayer.stop();
    },
    jump(time) {
      this.play(time);
    },
    play(time) {
      this.$refs.audioplayer.play(time);
      this.update();
    },
    applyTimestep() {
      this.timestep = this.newTimestep;
      this.reanalyze();
    },
    async reanalyze() {
      this.rendering = true;
      let req = await fetch(this.track);
      let buffer = await req.arrayBuffer();
      let audio = await decoder.decodeAudioData(buffer);
      this.audio = audio;

      let ctx = new OfflineAudioContext(2, audio.length, 44100);

      let source = ctx.createBufferSource();
      let proc = ctx.createScriptProcessor(4096, 2, 2)
      source.buffer = audio;
      source.connect(proc);
      proc.connect(ctx.destination);

      let ids = 0;
      let totalSamples = 0;
      let samplebuffer = [];
      let newWaveform = [];
      proc.onaudioprocess = (evt) => {
        samplebuffer.push(...evt.inputBuffer.getChannelData(0));

        let toSlice = Math.floor(44100 * this.timestep);
        while (samplebuffer.length > toSlice) {
          let buf = samplebuffer.splice(0, toSlice);
          totalSamples += toSlice;

          let rms = 0;
          for (let i = 0; i < buf.length; i++)
            rms += buf[i] ** 2;
          rms = Math.sqrt(rms / buf.length);

          let time = totalSamples / 44100;
          newWaveform.push({
            id: ++ids,
            time: time,
            rms: rms,
            marked: false
          });
        }
      }


      source.start();
      ctx.startRendering();
      ctx.oncomplete = () => {
        this.waveform.length = 0;
        this.waveform = newWaveform;
        this.rendering = false;

        setTimeout(() => {
          this.play(0);
          this.togglepause()
        }, 100);
      }
    }
  }
}
