export default function(app, getResolution, getZoom, onEventStart) {
  return interact('.timeline-object')
    .draggable({
      modifiers: [
        interact.modifiers.snap({
          targets: [interact.createSnapGrid({ x: 5, y: 5 })],
          relativePoints: [{ x: 0, y: 0 }],
          range: Infinity
        })
      ],
      inertia: true
    })
    .resizable({
      edges: {
        left: '.handle-left',
        right: '.handle-right',
        bottom: '.handle-bottom',
        top: '.handle-top'
      },
      modifiers: [
        interact.modifiers.restrictSize({
          min: { width: 5, height: 5 },
          max: { width: 360, height: Infinity }
        }),
        interact.modifiers.snap({
          targets: [interact.createSnapGrid({ x: 5, y: 5 })],
          range: Infinity,
          relativePoints: [{ x: 0, y: 0 }]
        })
      ]
    })
    .on('dragmove', event => {
      let i = +event.target.getAttribute('index');
      if (app.selected.indexOf(app.objects[i]) == -1) {
        app.clearSelection();
        app.selected.push(app.objects[i]);
        // console.log('dragmove override');
      }
      for (let selected of app.selected) {
        if (!event.ctrlKey) {
          selected.progress += event.dy * getResolution() / getZoom() * 1/5;
        }
        if (!event.shiftKey) {
          selected.start += event.dx;
          selected.end += event.dx;
        }
      }
    })
    .on('resizemove', event => {
      let i = +event.target.getAttribute('index');
      let obj = app.objects[i];
      if (app.selected.indexOf(obj) == -1) {
        app.clearSelection();
        app.selected.push(obj);
        // console.log('resizemove override');
      }
      for (let obj of app.selected) {
        const yfact = getResolution() / getZoom() * 1/5;
        obj.progress += event.deltaRect.top * yfact;
        obj.start += event.deltaRect.left;
        obj.length += (event.deltaRect.bottom - event.deltaRect.top) * yfact * getZoom();
        obj.end += event.deltaRect.right;
      }
    })
    .on('dragstart', () => {
      onEventStart('drag');
      app.dragselect.stop();
    })
    .on('resizestart', () => {
      onEventStart('resize');
      app.dragselect.stop();
    })
    .on('dragend', () => {
      for (let obj of app.selected) {
        obj.progress = Math.floor(obj.progress / getResolution()) * getResolution();
        obj.length = Math.floor(obj.length / getResolution()) * getResolution();
        obj.start = Math.floor(obj.start / 5) * 5;
        obj.end = Math.floor(obj.end / 5) * 5;
      }
      setTimeout(() => {
        app.dragselect.start();
        app.populateDragSelect();
      }, 10);
    })
    .on('resizeend', () => {
      for (let obj of app.selected) {
        obj.progress = Math.floor(obj.progress / getResolution()) * getResolution();
        obj.length = Math.floor(obj.length / getResolution()) * getResolution();
        obj.start = Math.floor(obj.start / 5) * 5;
        obj.end = Math.floor(obj.end / 5) * 5;
      }
      setTimeout(() => {
        app.dragselect.start();
        app.populateDragSelect();
      }, 10);
    });
}
