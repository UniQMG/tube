let keys = {
  free: false,
  w: false,
  s: false,
  a: false,
  d: false,
  q: false,
  e: false
};
const listeners = {
  free: [],
  drag: []
};

document.addEventListener('keydown', evt => keys[evt.key.toLowerCase()] = true);
document.addEventListener('keyup', evt => keys[evt.key.toLowerCase()] = false);
document.addEventListener('keypress', evt => {
  if (evt.key == ' ') {
    keys.free = !keys.free;
    for (let handler of listeners.free)
      handler(keys.free);
  }
});

let lx = null, ly = null;
document.addEventListener('mousedown', evt => {
  lx = evt.clientX, ly = evt.clientY;
});
document.addEventListener('mouseup', evt => {
  lx = null, ly = null;
});
document.addEventListener('mousemove', evt => {
  if (lx == null || ly == null) return;
  let dx = evt.clientX - lx, dy = evt.clientY - ly;
  lx = evt.clientX, ly = evt.clientY;

  for (let handler of listeners.drag)
    handler(dx, dy);
});

keys.on = function on(event, handler) {
  if (!listeners[event]) throw new Error('no');
  listeners[event].push(handler);
}

let gamepads = navigator.getGamepads();
window.addEventListener('gamepadconnected', evt => {
  gamepads = navigator.getGamepads();
});
keys.axes = function*(fast) {
  // Gamepads need polling on chrome, but leads to excessive
  // memory churn on firefox. Browser monopoly bad.
  if (navigator.userAgent.toLowerCase().indexOf("chrome") != -1)
    gamepads = navigator.getGamepads();

  yield fast
    ? keys.d - keys.a
    : { name: 'keyboard', axis: keys.d - keys.a };

  let gpid = 0;
  for (let gamepad of gamepads) {
    if (!gamepad) continue;
    gpid++;
    for (let i = 0; i < gamepad.axes.length; i += 2) {
      yield fast
        ? gamepad.axes[i]
        : { name: `xinput-${gpid}-axis-${i}`, axis: gamepad.axes[i] };
    }
  }
}

export default keys;
