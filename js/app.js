import Game from './game/game.js';
const html = arg => arg.join(''); // NOOP, for editor integration.

const canvas = document.getElementById('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const game = new Game(canvas);
game.refit();
window.game = game;
window.addEventListener('resize', () => game.refit());

import GameComp from './components/game.js';
import MenuComp from './components/menus/menu.js';
import SettingsComp from './components/menus/settings.js';
import AboutComp from './components/menus/about.js';

Vue.filter('decamel', text => {
  if (!text) return;
  text = text.toString().replace(/(.)([A-Z])/g, '$1 $2');
  text = text.slice(0, 1).toUpperCase() + text.slice(1).toLowerCase();
  return text;
});

const app = new Vue({
  template: html`
    <component
      id="content"
      :class="'tab-' + activeTab"
      :is="tab.component"
      v-bind="tab.props"
      @navigate="navigate"
    ></component>
  `,
  computed: {
    tab() {
      return this.tabs.filter(tab => tab.name == this.activeTab)[0];
    }
  },
  methods: {
    navigate(to) {
      this.activeTab = to;
      let classlist = document.body.classList;
      classlist.toggle('playing', this.tab.component == GameComp);
      classlist.toggle('hidebackground', this.tab.component == SettingsComp);
    }
  },
  data: {
    activeTab: 'mainmenu',
    tabs: [{
      name: 'mainmenu',
      component: MenuComp,
      props: { game },
    }, {
      name: 'game',
      component: GameComp,
      props: { game }
    }, {
      name: 'editor',
      component: GameComp,
      props: { game, externalEditor: true }
    }, {
      name: 'settings',
      component: SettingsComp,
      props: { settings: game.settings }
    }, {
      name: 'about',
      component: AboutComp,
      props: {}
    }]
  }
});
app.$mount('#app');

window.app = app;
export default app;
